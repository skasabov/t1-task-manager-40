package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @NotNull
    List<Session> findAll();

    @NotNull
    Session add(@Nullable Session model);

    @NotNull
    Collection<Session> addAll(@Nullable Collection<Session> models);

    @NotNull
    Collection<Session> set(@Nullable Collection<Session> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    Session findOneById(@Nullable String id);

    @Nullable
    Session findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    Session removeOne(@Nullable Session model);

    @Nullable
    Session removeOneById(@Nullable String id);

    @Nullable
    Session removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<Session> collection);

    void removeAll();

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Session> findAll(@Nullable String userId);

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Session findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    Session removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Session removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

}
