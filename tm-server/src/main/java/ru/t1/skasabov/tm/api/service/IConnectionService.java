package ru.t1.skasabov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull
    SqlSession getSqlSession();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    void initDatabase();

}
