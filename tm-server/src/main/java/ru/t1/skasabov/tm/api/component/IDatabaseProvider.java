package ru.t1.skasabov.tm.api.component;

import org.apache.ibatis.annotations.Update;

public interface IDatabaseProvider {

    @Update("create table if not exists tm_project (" +
            "id varchar(255) not null, " +
            "created timestamp not null default current_date, " +
            "name varchar(255) not null, " +
            "description varchar(255), " +
            "status varchar(255) not null default 'NOT_STARTED', " +
            "user_id varchar(255), " +
            "begin_date timestamp, " +
            "end_date timestamp, " +
            "primary key (id), " +
            "constraint fk_user foreign key(user_id) references tm_user(id) on delete cascade" +
            ")")
    void createProjectTable();

    @Update("create table if not exists tm_task (" +
            "id varchar(255) not null, " +
            "created timestamp not null default current_date, " +
            "name varchar(255) not null, " +
            "description varchar(255), " +
            "status varchar(255) not null default 'NOT_STARTED', " +
            "user_id varchar(255), " +
            "project_id varchar(255), " +
            "begin_date timestamp, " +
            "end_date timestamp, " +
            "primary key (id), " +
            "constraint fk_user foreign key(user_id) references tm_user(id) on delete cascade, " +
            "constraint fk_project foreign key(project_id) references tm_project(id) on delete cascade" +
            ")")
    void createTaskTable();

    @Update("create table if not exists tm_user (" +
            "id varchar(255) not null, " +
            "login varchar(255) not null, " +
            "password varchar(255) not null, " +
            "email varchar(255), " +
            "last_name varchar(255), " +
            "first_name varchar(255), " +
            "middle_name varchar(255), " +
            "locked boolean not null default false, " +
            "role varchar(255) not null default 'USUAL', " +
            "primary key (id)" +
            ")")
    void createUserTable();

    @Update("create table if not exists tm_session (" +
            "id varchar(255) not null, " +
            "created timestamp not null default current_date, " +
            "role varchar(255) not null default 'USUAL', " +
            "user_id varchar(255), " +
            "primary key (id), " +
            "constraint fk_user foreign key(user_id) references tm_user(id) on delete cascade" +
            ")")
    void createSessionTable();

}
