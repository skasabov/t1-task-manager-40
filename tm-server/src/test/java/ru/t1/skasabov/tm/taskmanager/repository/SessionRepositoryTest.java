package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.ISessionRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private SqlSession sqlSession;

    @Before
    @SneakyThrows
    public void initRepository() {
        sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        sessionRepository.removeAll();
        @NotNull final User userOne = new User();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        @NotNull final User userTwo = new User();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_ONE);
            else session.setUserId(USER_ID_TWO);
            sessionRepository.add(session);
        }
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearAll() {
        sessionRepository.removeAll();
        Assert.assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        sessionRepository.removeAllForUser(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionList = sessionRepository.findAllForUser(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test
    public void testFindById() {
        @NotNull final Session session = sessionRepository.findAll().get(0);
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findOneById(sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Session session = sessionRepository.findAllForUser(USER_ID_ONE).get(0);
        @NotNull final String sessionId = sessionRepository.findAllForUser(USER_ID_ONE).get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findOneByIdForUser(USER_ID_ONE, sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIdSessionNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindByIdSessionNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneByIdForUser(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Session session = sessionRepository.findAll().get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIndexSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(0));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Session session = sessionRepository.findAllForUser(USER_ID_TWO).get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndexForUser(USER_ID_TWO, 0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIndexForUserSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndexForUser(USER_ID_ONE, 0));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_ONE);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Session session = new Session();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionRepository.getSizeForUser(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(invalidId));
        Assert.assertTrue(sessionRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionRepository.findAllForUser(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsByIdForUser(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionRepository.existsByIdForUser(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = sessionRepository.getSizeForUser(USER_ID_ONE) - 1;
        @NotNull final Session session = sessionRepository.findAllForUser(USER_ID_ONE).get(0);
        sessionRepository.removeOne(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSizeForUser(USER_ID_ONE));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - 1;
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        sessionRepository.removeOneById(sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSizeForUser(USER_ID_ONE) - 1;
        @NotNull final String sessionId = sessionRepository.findAllForUser(USER_ID_ONE).get(0).getId();
        sessionRepository.removeOneByIdForUser(USER_ID_ONE, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSizeForUser(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = sessionRepository.getSize() - 1;
        sessionRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndexSessionNotFound() {
        sessionRepository.removeAll();
        sessionRepository.removeOneByIndex(0);
        Assert.assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = sessionRepository.getSizeForUser(USER_ID_TWO) - 1;
        sessionRepository.removeOneByIndexForUser(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSizeForUser(USER_ID_TWO));
    }

    @Test
    public void testRemoveByIndexForUserSessionNotFound() {
        sessionRepository.removeAll();
        sessionRepository.removeOneByIndexForUser(USER_ID_ONE, 0);
        Assert.assertEquals(0, sessionRepository.getSizeForUser(USER_ID_ONE));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        sqlSession.rollback();
        sqlSession.close();
    }

}
